terraform {
  backend "s3" {

    bucket = "terraform-eks-cicd-new-1"
    key = "state"
    region = "us-east-1"
    
  }
}